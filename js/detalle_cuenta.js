/**
 * Created by Daniel on 17-Dec-14.
 */
var tipo_compromiso;
var datos_tabla;

$(document).ready(function() {
    $('.datos_tabla').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar: ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        } ]
    });

    //setInterval(refrescarIndice, 500);

});

$('.datos_tabla tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla').DataTable().row( this ).data();
    $("#conciliar_movimiento").val(datos_tabla[0]);
    $("#cancelar_movimiento").val(datos_tabla[1]);
});

$('.modal_borrar').on('show.bs.modal', function (event) {
    var modal = $(this);
    modal.find('#cancelar_compromiso').val(datos_tabla[0]);
});

$("#elegir_cancelar_movimiento").click(function() {
    var movimiento = $("#cancelar_movimiento").val();

    $.ajax({
        type: "POST",
        //dataType: "json",
        url: "/ciclo/cancelarMovimientoCaratula",
        data: {
            movimiento: movimiento
        }
    })
        .done(function( data ) {
            console.log(data);
            if(data) {
                refrescarIndice();
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});

$("#elegir_conciliar_movimiento").click(function() {
    var movimiento = $("#conciliar_movimiento").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/conciliar_movimiento",
        data: {
            movimiento: movimiento
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                refrescarIndice();
            }
            else {
                alert("Ha ocurrido un error");
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$('.modal_poliza').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#numero_movimiento_poliza').html(''+datos_tabla[1]);
    modal.find('#numero_movimiento_poliza_hidden').val(datos_tabla[1]);
});

$("#elegir_generar_poliza").click(function() {
    var movimiento = datos_tabla[1];

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/generar_poliza_movimiento_presupuesto",
        data: {
            movimiento: movimiento
        }
    })
        .done(function( data ) {
            $("#mensaje_resultado_poliza").html(data.mensaje);
            $('.resultado_poliza').modal('show');
            refrescarIndice();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

function refrescarIndice() {
    var url = js_base_url('ciclo/tabla_principal_cuenta');
    var cuenta = $("#cuenta").val();

    $.ajax({
        url: url,
        dataType: 'json',
        method: "POST",
        data: {
            id_cuenta: cuenta
        },
        success: function(s){
            $('.datos_tabla').dataTable().fnClearTable();
            for(var i = 0; i < s.length; i++) {
                $('.datos_tabla').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    s[i][6],
                    s[i][7],
                    s[i][8],
                    s[i][9],
                    s[i][10]
                ]);
            } // End For

        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

$("#crear_movimientos").on("click", function() {
    $("#forma_tipo_movimiento").submit();
});

$(".boton_conciliar").click(function() {
    console.log("ID: "+datos_tabla[0]);
    alert("ID: "+datos_tabla[0]);
});

//$("#elegir_borrar_matriz").click(function() {
//    var matriz = $("#ultimo_movimiento").val();
//
//    var table = $('#tabla_detalle').DataTable();
//
//    $.ajax({
//        type: "POST",
//        dataType: "json",
//        url: "tabla_detalle_movimiento",
//        data: {
//            matriz: matriz
//        }
//    })
//        .done(function( data ) {
//            console.log(data);
//            if(data.mensaje == "ok") {
//                table.ajax.reload();
//            }
//            else {
//                alert("Ha ocurrido un error");
//            }
//        })
//        .fail(function(e) {
//            // If there is no communication between the server, show an error
//            console.log("Error");
//            //alert( "error occured" );
//        });
//});