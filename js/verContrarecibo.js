var datos_tabla_contrarecibo;

$(document).ready(function() {
    var tipo = $("#destino").val();

    if(tipo == "Fondo Revolvente" || tipo == "Gastos a Comprobar" || tipo == "Viáticos Nacionales" || tipo == "Viáticos Internacionales") {
        $(".forma_normal").hide();
        $(".forma_diferente").show();
    }

    $('#tabla_datos_contrarecibo').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 7 ],
            "visible": false,
            "searchable": false
        }
        ]
    });

    refrescarDetalle();

    //setInterval(refrescarDetalle, 500);

});

function refrescarDetalle() {
    $.ajax({
        url: '/ciclo/tabla_detalle_contrarecibo_AJAX',
        dataType: 'json',
        method: 'POST',
        data: {
            contrarecibo: $("#ultimo_contrarecibo").val()
        },
        success: function(s){
            $('#tabla_datos_contrarecibo').dataTable().fnClearTable();
            var subtotal = 0;
            var iva = 0;
            var total = 0;
            for(var i = 0; i < s.length; i++) {
                subtotal += parseFloat(s[i][3]);
                iva += parseFloat(s[i][4]);
                total += parseFloat(s[i][5]);
                $('#tabla_datos_contrarecibo').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    "$"+$.number( s[i][3], 2 ),
                    "$"+$.number( s[i][4], 2 ),
                    "$"+$.number( s[i][5], 2 ),
                    s[i][6],
                    s[i][7]
                ]);
            } // End For

            var destino = $("#destino").html();

            if(destino == "Honorarios") {
                var total_pagar = total;
                var isr = subtotal * .10;
                var iva_retenido = (iva / 3) * 2;
                total_pagar -= isr;
                total_pagar -= iva_retenido;
                $("#desglose_honorarios").html(
                    '<div class="col-lg-12"> ' +
                    '<div class="panel panel-default"> ' +
                    '<div class="panel-heading">' +
                    'Desglose de Honorarios' +
                    '</div> ' +
                    '<div class="panel-body"> ' +
                    '<div class="row"> ' +
                    '<div class="col-lg-3"></div>'+
                    '<div class="col-lg-3"> ' +
                    '<h5>Monto de los honorarios</h5> ' +
                    '<h5>IVA 16%</h5> ' +
                    '<hr>'+
                    '<h5><b>Subtotal</b></h5> ' +
                    '<hr>'+
                    '<h5>ISR 10%</h5> ' +
                    '<h5>IVA Retenido</h5> ' +
                    '<hr>'+
                    '<h4 style="background:#f8f8f8; padding: 4%;">Total a pagar</h4> ' +
                    '</div> ' +
                    '<div class="col-lg-3"> ' +
                    '<h5>$'+$.number( subtotal, 2 )+'</h5> ' +
                    '<h5>$'+$.number( iva, 2 )+'</h5> ' +
                    '<hr>'+
                    '<h5><b>$'+$.number( total, 2 )+'</b></h5> ' +
                    '<hr>'+
                    '<h5>$'+$.number( isr, 2 )+'</h5> ' +
                    '<h5>$'+$.number( iva_retenido, 2 )+'</h5> ' +
                    '<hr>'+
                    '<h4  style="background:#f8f8f8; padding: 4%;">$'+$.number( total_pagar , 2 )+'</h4> ' +
                    '</div> ' +
                    '<div class="col-lg-3"></div>'+
                    '</div> ' +
                    '</div> ' +
                    '</div>');
            }

            $("#suma_total").html("Total"+ "<span style='color:#848484;'>" + " $ " +$.number( total, 2 )+ "</span>");

            $("#total_hidden").val(total);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}