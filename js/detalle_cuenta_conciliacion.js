var tipo_compromiso;
var datos_tabla;

var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth() + 1;
var curr_year = d.getFullYear();

//Esta funcion es para traducir el datepicker
$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

$( "#fecha_conciliar" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: '2015-01-01' });

$(document).ready(function() {
    $('.datos_tabla').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar: ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        },
        "columnDefs": [ {
            "targets": [ 0, 1 ],
            "visible": false,
            "searchable": false
        } ]
    });

    $("#fecha_conciliar").val(curr_year + "-" + curr_month + "-" + curr_date);

});

$('.datos_tabla tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla').DataTable().row( this ).data();
    $("#conciliar_movimiento").val(datos_tabla[0]);
    $("#quitar_conciliar_movimiento").val(datos_tabla[0]);
});

$('.modal_borrar').on('show.bs.modal', function (event) {
    var modal = $(this);
    modal.find('#cancelar_compromiso').val(datos_tabla[0]);
});

$("#elegir_conciliar_movimiento").click(function() {
    var movimiento = $("#conciliar_movimiento").val();
    var fecha = $("#fecha_conciliar").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/conciliar_movimiento",
        data: {
            movimiento: movimiento,
            fecha: fecha
        }
    })
        .done(function( data ) {
            if(data.mensaje == "ok") {
                refrescarIndice();
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});

$("#elegir_quitar_conciliar_movimiento").click(function() {
    var movimiento = $("#quitar_conciliar_movimiento").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/quitar_conciliar_movimiento",
        data: {
            movimiento: movimiento
        }
    })
        .done(function( data ) {
            console.log(data);
            if(data.mensaje == "ok") {
                refrescarIndice();
            }
        })
        .fail(function(e) {
            // If there is no communication between the server, show an error
            console.log(e.responseText);
            //alert( "error occured" );
        });
});

function refrescarIndice() {
    var url = js_base_url('ciclo/tabla_principal_cuenta_conciliacion');
    var cuenta = $("#cuenta").val();

    $.ajax({
        url: url,
        dataType: 'json',
        method: "POST",
        data: {
            id_cuenta: cuenta
        },
        success: function(s){
            $('.datos_tabla').dataTable().fnClearTable();
            for(var i = 0; i < s.length; i++) {
                $('.datos_tabla').dataTable().fnAddData([
                    s[i][0],
                    s[i][1],
                    s[i][2],
                    s[i][3],
                    s[i][4],
                    s[i][5],
                    s[i][6],
                    s[i][7],
                    s[i][8],
                    s[i][9],
                    s[i][10]
                ]);
            } // End For

        },
        error: function(e){
            console.log(e.responseText);
        }
    });
}

$("#crear_movimientos").on("click", function() {
    $("#forma_tipo_movimiento").submit();
});

$(".boton_conciliar").click(function() {
    console.log("ID: "+datos_tabla[0]);
    alert("ID: "+datos_tabla[0]);
});