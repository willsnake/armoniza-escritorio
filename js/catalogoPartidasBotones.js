var editor;

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        "table": "#datos_tabla",
        "fields": [ {
            "label": "Partida:",
            "name": "partida"
        }, {
            "label": "Descripci&oacute;n:",
            "name": "descripcion"
        }
        ],
        i18n: {
            create: {
                button: "<i class='fa fa-plus-circle circle ic-catalogo'></i> Nuevo",
                title:  "<i class='fa fa-plus-circle ic-catalogo-emergente'></i> Nuevo Partida",
                submit: "Ingresar"
            },
            edit: {
                button: "<i class='fa fa-edit circle ic-catalogo'></i> Editar",
                title:  "<i class='fa fa-edit ic-catalogo-emergente'></i> Editar Partida",
                submit: "Actualizar"
            },
            remove: {
                button: "<i class='fa fa-trash-o circle ic-catalogo'></i> Borrar",
                title:  "<i class='fa fa-trash-o ic-catalogo-emergente'></i> Borrar Partida",
                submit: "Borrar",
                confirm: {
                    _: "&iquest;Esta seguro que desea eliminar la Partida seleccionada ?",
                    1: "&iquest;Esta seguro que desea eliminar la Partida seleccionada ?"
                }
            },
            error: {
                system: "Ha ocurrido un error, por favor, contacte al administrador del sistema."
            }
        }
    } );

    $('#datos_tabla').DataTable( {
        dom:"Bl<frtip>",
        columns: [
            { data: "partida" },
            { data: "descripcion" }
        ],
        select: true,
        buttons: [
            { extend: "create", editor: editor },
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: editor }
        ],
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning�n dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "�ltimo",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    } );
} );
