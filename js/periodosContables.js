var datos_tabla_periodo;
//Esta funcion es para traducir el datepicker
$(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

$( "#fecha_inicio" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#fecha_termina" ).datepicker({ dateFormat: 'yy-mm-dd' });

$(document).ready(function() {

    $('.datos_tabla').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         '',
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
            "columnDefs": [ {
            "targets": [ 0 ],
            "visible": false,
            "searchable": false
        }],

    });

    $('#tabla_cuentas_afectadas').dataTable({
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "",
                "sPrevious": ""
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

});

$('.datos_tabla tbody').on( 'click', 'tr', function () {

    datos_tabla_periodo = $('.datos_tabla').DataTable().row( this ).data();
    $('#id_periodo_editar').val(datos_tabla_periodo[0]);

    if(datos_tabla_periodo[8] == "Si") {
        $("#periodo1").prop('checked', true);
    } else {
        $("#periodo2").prop('checked', true);
    }

    mostrarCuentasAfectadas(datos_tabla_periodo[2], datos_tabla_periodo[3]);

});

$("#forma_periodo").on( "submit", function(e) {

    e.preventDefault();

    var table = $('.datos_tabla').DataTable();

    var map = {};
    $(":input").each(function () {
        map[$(this).attr("name")] = $(this).val();
    });

    $.ajax({
        url: '/contabilidad/agregar_periodo',
        method: 'POST',
        dataType: 'json',
        data: map,
        beforeSend: function (data) {
            $('#imagen_espera').html( '<img src="'+js_base_url("img/ajax-loader2.gif")+'" style="width: 20%;" />');
        },
        success: function (s) {
            $('#imagen_espera').html('');
            $('#agregarPeriodo').modal('hide');
            $("#resultado_periodo").html(s.mensaje);
            $('#resultado_mensaje').modal('show');
            table.ajax.reload();
        },
        error: function (e) {
            console.log(e.responseText);
        }
    });

});

$("#elegir_editar_periodo").click(function() {
    var id_periodo = $("#id_periodo_editar").val();
    var cerrado = $("input[name='periodo']:checked").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/contabilidad/editar_periodo_contable",
        data: {
            periodo: id_periodo,
            cerrado: cerrado
        }
    })
        .done(function( data ) {
            table.ajax.reload();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$("#cerrar_periodo").on( "submit", function(e) {

    e.preventDefault();

    var table = $('.datos_tabla').DataTable();

    var map = {};
    $(":input").each(function () {
        map[$(this).attr("name")] = $(this).val();
    });

    $.ajax({
        url: '/contabilidad/cerrar_periodo_anual',
        method: 'POST',
        dataType: 'json',
        data: map,
        beforeSend: function (data) {
            $('#imagen_espera').html( '<img src="'+js_base_url("img/ajax-loader2.gif")+'" style="width: 20%;" />');
        },
        success: function (s) {
            console.log(s);
            $('#imagen_espera').html('');
            $('#agregarPeriodo').modal('hide');
            $("#resultado_periodo").html(s.mensaje);
            $('#resultado_mensaje').modal('show');
            table.ajax.reload();
        },
        error: function (e) {
            console.log(e.responseText);
        }
    });

});

$("#cerrar_periodo_anual").on( "click", function(e) {

    $.ajax({
        url: '/contabilidad/cerrar_periodo_anual',
        method: 'POST',
        dataType: 'json',
        beforeSend: function (data) {
            $('#logo_espera').html( '<img src="'+js_base_url("img/ajax-loader2.gif")+'" style="width: 20%;" />');
        },
        success: function (s) {

            if(s.mensaje.indexOf("success") > -1) {
                $('#cerrarPeriodoAnualBoton').hide();
            }
            $("#resultado_periodo").html(s.mensaje);

            $('#cerrarperiodoanual').modal('hide');

            $('#resultado_mensaje').modal('show');

            $('#logo_espera').html( '');

            revisarPolizasCierreAnual();
        },
        error: function (e) {
            console.log(e.responseText);
        }
    });

});

function mostrarCuentasAfectadas(fecha_inicial, fecha_final) {

    $.ajax({
        url: '/contabilidad/tabla_cuentas_periodos_contables',
        method: 'POST',
        data: {
            fecha_inicial: fecha_inicial,
            fecha_final: fecha_final
        },
        beforeSend: function (data) {
            $('#imagen_espera_cuentas').html( '<img src="'+js_base_url("img/ajax-loader2.gif")+'" style="width: 5%;" />');
        },
        success: function (s) {
            $('#imagen_espera_cuentas').html('');
            $('#cuerpo_tabla_cuentas_afectadas').html(s);
        },
        error: function (e) {
            console.log(e.responseText);
        }
    });
}

function revisarPolizasCierreAnual() {

    $.ajax({
        url: '/contabilidad/revisar_tabla_cierre_anual',
        method: 'POST',
        dataType: 'json',
        success: function (s) {

            if(s.mensaje == "mostrar") {
                $('#botones_exportar').show();
                $('#cerrarPeriodoAnualBoton').hide();
            } else {
                $('#cerrarPeriodoAnualBoton').show();
            }

        },
        error: function (e) {
            console.log(e.responseText);
        }

    });
}

