var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        table: "#tabla_proveedores",
        fields: [{
            label: "Clave",
            name: "clave_prov"
        }, {
            label: "Nombre",
            name: "nombre"
        }, {
            label: "Calle",
            name: "calle"
        }, {
            label: "Teléfono",
            name: "telefono"
        }, {
            label: "No. Exterior",
            name: "no_exterior"
        }, {
            label: "No. Interior",
            name: "no_interior"
        }, {
            label: "Colonia",
            name: "colonia"
        }, {
            label: "C.P.",
            name: "cp"
        }, {
            label: "Ciudad",
            name: "ciudad"
        }, {
            label: "Delegación / Municipio",
            name: "delegacion_municipio"
        }, {
            label: "País",
            name: "pais"
        }, {
            label: "Email",
            name: "email"
        }, {
            label: "RFC",
            name: "RFC"
        }, {
            label: "No. Cuenta",
            name: "cuenta"
        }, {
            label: "CLABE Bancaria",
            name: "clabe"
        }, {
            label: "Nombre del Banco",
            name: "banco"
        }, {
            label: "Forma de Pago",
            name: "pago"
        }, {
            label: "PyMe:",
            name:  "mypyme"
        }, {
            label: "Observaciones",
            name: "observaciones"
        } ],
        i18n: {
            create: {
                button: "<i class='fa fa-plus-circle circle ic-catalogo'></i> Nuevo",
                title:  "<i class='fa fa-plus-circle ic-catalogo-emergente'></i> Nuevo Proveedor",
                submit: "Ingresar"
            },
            edit: {
                button: "<i class='fa fa-edit circle ic-catalogo'></i> Editar",
                title:  "<i class='fa fa-edit ic-catalogo-emergente'></i> Editar Proveedor",
                submit: "Actualizar"
            },
            remove: {
                button: "<i class='fa fa-trash-o circle ic-catalogo'></i> Borrar",
                title:  "<i class='fa fa-trash-o ic-catalogo-emergente'></i> Borrar",
                submit: "Borrar",
                confirm: {
                    _: "¿Esta seguro que desea eliminar los %d proveedores seleccionados?",
                    1: "¿Esta seguro que desea eliminar el proveedor seleccionado?"
                }
            },
            error: {
                system: "Ha ocurrido un error, por favor, contacte al administrador del sistema."
            }
        }
    } );

    editor.on( 'initCreate', function (e){
        editor.disable('clave_prov');
        $.ajax({
            url: '/catalogos/cadena_random_proveedor',
            dataType: 'json',
            success: function(s){
                editor.set( 'clave_prov', s.respuesta );
            },
            error: function(e){
                console.log(e.responseText);
            }
        });
    });

    $('#tabla_proveedores').DataTable( {
        dom: 'Tlfrtip',
        tableTools: {
            "sSwfPath": "./assets/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [
                {
                    "sExtends": "copy",
                    "sButtonText": "Copy to clipboard"
                },
                {
                    "sExtends": "csv",
                    "sButtonText": "Save to CSV"
                },
                {
                    "sExtends": "xls",
                    "oSelectorOpts": {
                        page: 'current'
                    }
                }
            ]
        },
        columns: [
            { data: "clave_prov",
                type: "readonly" },
            { data: "nombre" },
            { data: "calle" },
            { data: "no_exterior" },
            { data: "no_interior" },
            { data: "colonia" },
            { data: "telefono" },
            { data: "RFC" },
            { data: "cuenta" },
            { data: "clabe" },
            {
                "data": "mypyme",
                "render": function (val, type, row) {
                    return val == 0 ? "No" : "Si";
                }
            }
        ],
        tableTools: {
            sRowSelect: "os",
            aButtons: [
                { sExtends: "editor_create", editor: editor },
                { sExtends: "editor_edit",   editor: editor },
                { sExtends: "editor_remove", editor: editor }
            ]
        },
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    } );
} );