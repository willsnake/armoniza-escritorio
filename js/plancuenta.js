var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        ajax: "/contabilidad/tabla_plan_cuentas",
        table: ".datos_tabla",
        fields: [ {
            label: "Nombre",
            name: "nombre"
        }, {
            label: "Calle",
            name: "calle"
        }, {
            label: "Teléfono",
            name: "telefono"
        }, {
            label: "No. Exterior",
            name: "no_exterior"
        }, {
            label: "No. Interior",
            name: "no_interior"
        }, {
            label: "Colonia",
            name: "colonia"
        }, {
            label: "C.P.",
            name: "cp"
        }, {
            label: "Ciudad",
            name: "ciudad"
        }, {
            label: "Delegación / Municipio",
            name: "delegacion_municipio"
        }, {
            label: "País",
            name: "pais"
        }, {
            label: "Email",
            name: "email"
        }, {
            label: "RFC",
            name: "RFC"
        }, {
            label: "No. Cuenta",
            name: "cuenta"
        }, {
            label: "CLABE Bancaria",
            name: "clabe"
        }, {
            label: "Nombre del Banco",
            name: "banco"
        }, {
            label: "Forma de Pago",
            name: "pago",
            type:  "select",
            options: [
                { label: "Transferencia",   value: "TRANSFERENCIA" },
                { label: "Cheque",          value: "CHEQUE" },
                { label: "Deposio",        value: "DEPOSITO" },
                { label: "Efectivo",        value: "EFECTIVO" }
            ]
        }, {
            label: "MyPyme",
            name: "mypyme",
            type:  "radio",
            options: [
                { label: "No", value: 0 },
                { label: "Si",  value: 1 }
            ],
            "default": 0
        }, {
            label: "Observaciones",
            name: "observaciones"
        } ],
        i18n: {
            create: {
                button: "Nuevo",
                title:  "Ingresar Nuevo Proveedor",
                submit: "Ingresar"
            },
            edit: {
                button: "Editar",
                title:  "Editar Proveedor",
                submit: "Actualizar"
            },
            remove: {
                button: "Borrar",
                title:  "Borrar",
                submit: "Borrar",
                confirm: {
                    _: "¿Esta seguro que desea eliminar los %d proveedores seleccionados?",
                    1: "¿Esta seguro que desea eliminar el proveedor seleccionado?"
                }
            },
            error: {
                system: "Ha ocurrido un error, por favor, contacte al administrador del sistema."
            }
        }
    } );

    $('#example').DataTable( {
        dom: "Tfrtip",
        ajax: {
            url: "../php/staff.php",
            type: "POST"
        },
        serverSide: true,
        columns: [
            { data: "first_name" },
            { data: "last_name" },
            { data: "position" },
            { data: "office" },
            { data: "start_date" },
            { data: "salary", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
        ],
        tableTools: {
            sRowSelect: "os",
            aButtons: [
                { sExtends: "editor_create", editor: editor },
                { sExtends: "editor_edit",   editor: editor },
                { sExtends: "editor_remove", editor: editor }
            ]
        }
    } );

    $('.datos_tabla').DataTable( {
        dom: "Tfrtip",
        ajax: {
            url: "/contabilidad/tabla_proveedores",
            type: "POST"
        },
        serverSide: true,
        columns: [
            { data: "id_proveedores" },
            { data: "nombre" },
            { data: null, render: function ( data, type, row ) {
                if(data.no_interior != null) {
                    return data.calle+' '+data.no_exterior+' '+data.no_interior+' '+data.colonia+' '+data.cp+' '+data.delegacion_municipio;
                } else {
                    return data.calle+' '+data.no_exterior+' '+data.colonia+' '+data.cp+' '+data.delegacion_municipio;
                }

            } },
            { data: "telefono" },
            { data: "RFC" },
            { data: "cuenta" }
        ],
        tableTools: {
            sRowSelect: "os",
            aButtons: [
                { sExtends: "editor_create", editor: editor },
                { sExtends: "editor_edit",   editor: editor },
                { sExtends: "editor_remove", editor: editor }
            ]
        },
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    } );
} );