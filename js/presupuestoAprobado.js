var editor;
var datos_tabla;
var check_cerrado = 0;

$(document).ready(function() {

    $.ajax({
        url: '/anteproyecto/cerrar_aprobado',
        method: 'POST',
        dataType: 'json',
        success: function(s){
            check_cerrado = s;
            renderTabla(check_cerrado);
        }
    });

});

function renderTabla(check) {
    editor = new $.fn.dataTable.Editor({
        "table": "#datos_tabla",
        "fields": [{
            label: "Centro de Costos",
            name: "centro_costos"
        }, {
            label: "Descripcion Centro de Costos",
            name: "descripcion_centro_costos"
        }, {
            label: "Partida",
            name: "partida"
        }, {
            label: "Descripcion Partida",
            name: "descripcion_partida"
        }, {
            label: "Enero 2015",
            name: "enero_anio"
        }, {
            label: "Enero Fondos 2015",
            name: "enero_fondos"
        }, {
            label: "Enero 2016 Solicitado",
            name: "enero_solicitado"
        }, {
            label: "Enero 2016 Aprobado",
            name: "enero_aprobado"
        }, {
            label: "Febrero 2015",
            name: "febrero_anio"
        }, {
            label: "Febrero Fondos 2015",
            name: "febrero_fondos"
        }, {
            label: "Febrero 2016 Solicitado",
            name: "febrero_solicitado"
        }, {
            label: "Febrero 2016 Aprobado",
            name: "febrero_aprobado"
        }, {
            label: "Marzo 2015",
            name: "marzo_anio"
        }, {
            label: "Marzo Fondos 2015",
            name: "marzo_fondos"
        }, {
            label: "Marzo 2016 Solicitado",
            name: "marzo_solicitado"
        }, {
            label: "Marzo 2016 Aprobado",
            name: "marzo_aprobado"
        }, {
            label: "Abril 2015",
            name: "abril_anio"
        }, {
            label: "Abril Fondos 2015",
            name: "abril_fondos"
        }, {
            label: "Abril 2016 Solicitado",
            name: "abril_solicitado"
        }, {
            label: "Abril 2016 Aprobado",
            name: "abril_aprobado"
        }, {
            label: "Mayo 2015",
            name: "mayo_anio"
        }, {
            label: "Mayo Fondos 2015",
            name: "mayo_fondos"
        }, {
            label: "Mayo 2016 Solicitado",
            name: "mayo_solicitado"
        }, {
            label: "Mayo 2016 Aprobado",
            name: "mayo_aprobado"
        }, {
            label: "Junio 2015",
            name: "junio_anio"
        }, {
            label: "Junio Fondos 2015",
            name: "junio_fondos"
        }, {
            label: "Junio 2016 Solicitado",
            name: "junio_solicitado"
        }, {
            label: "Junio 2016 Aprobado",
            name: "junio_aprobado"
        }, {
            label: "Julio 2015",
            name: "julio_anio"
        }, {
            label: "Julio Fondos 2015",
            name: "julio_fondos"
        }, {
            label: "Julio 2016 Solicitado",
            name: "julio_solicitado"
        }, {
            label: "Julio 2016 Aprobado",
            name: "julio_aprobado"
        }, {
            label: "Agosto 2015",
            name: "agosto_anio"
        }, {
            label: "Agosto Fondos 2015",
            name: "agosto_fondos"
        }, {
            label: "Agosto 2016 Solicitado",
            name: "agosto_solicitado"
        }, {
            label: "Agosto 2016 Aprobado",
            name: "agosto_aprobado"
        }, {
            label: "Septiembre 2015",
            name: "septiembre_anio"
        }, {
            label: "Septiembre Fondos 2015",
            name: "septiembre_fondos"
        }, {
            label: "Septiembre 2016 Solicitado",
            name: "septiembre_solicitado"
        }, {
            label: "Septiembre 2016 Aprobado",
            name: "septiembre_aprobado"
        }, {
            label: "Octubre 2015",
            name: "octubre_anio"
        }, {
            label: "Octubre Fondos 2015",
            name: "octubre_fondos"
        }, {
            label: "Octubre 2016 Solicitado",
            name: "octubre_solicitado"
        }, {
            label: "Octubre 2016 Aprobado",
            name: "octubre_aprobado"
        }, {
            label: "Noviembre 2015",
            name: "noviembre_anio"
        }, {
            label: "Noviembre Fondos 2015",
            name: "noviembre_fondos"
        }, {
            label: "Noviembre 2016 Solicitado",
            name: "noviembre_solicitado"
        }, {
            label: "Noviembre 2016 Aprobado",
            name: "noviembre_aprobado"
        }, {
            label: "Diciembre 2015",
            name: "diciembre_anio"
        }, {
            label: "Diciembre Fondos 2015",
            name: "diciembre_fondos"
        }, {
            label: "Diciembre 2016 Solicitado",
            name: "diciembre_solicitado"
        }, {
            label: "Diciembre 2016 Aprobado",
            name: "diciembre_aprobado"
        }, {
            label: "Total 2015",
            name: "total_anio"
        }, {
            label: "Total Fondos",
            name: "total_fondos"
        }, {
            label: "Total Solicitado",
            name: "total_solicitado"
        }, {
            label: "Total Aprobado",
            name: "total_aprobado"
        }
        ],
        i18n: {
            create: {
                button: "<i class='fa fa-plus-circle circle ic-catalogo'></i> Nuevo",
                title: "<i class='fa fa-plus-circle ic-catalogo-emergente'></i> Nuevo Plan de Cuenta Contable",
                submit: "Ingresar"
            },
            edit: {
                button: "<i class='fa fa-edit circle ic-catalogo'></i> Editar",
                title: "<i class='fa fa-edit ic-catalogo-emergente'></i> Editar Plan de Cuenta Contable",
                submit: "Actualizar"
            },
            remove: {
                button: "<i class='fa fa-trash-o circle ic-catalogo'></i> Borrar",
                title: "<i class='fa fa-trash-o ic-catalogo-emergente'></i> Borrar",
                submit: "Borrar",
                confirm: {
                    _: "�Esta seguro que desea eliminar los %d Planes de Cuentas Contables seleccionados?",
                    1: "�Esta seguro que desea eliminar el Plan de Cuenta Contable seleccionado?"
                }
            },
            error: {
                system: "Ha ocurrido un error, por favor, contacte al administrador del sistema."
            }
        }
    });

    if(check_cerrado == 0) {
        editor.disable([
            'centro_costos',
            'descripcion_centro_costos',
            'partida',
            'descripcion_partida',
            'enero_anio',
            'enero_fondos',
            'enero_solicitado',
            'febrero_anio',
            'febrero_fondos',
            'febrero_solicitado',
            'marzo_anio',
            'marzo_fondos',
            'marzo_solicitado',
            'abril_anio',
            'abril_fondos',
            'abril_solicitado',
            'mayo_anio',
            'mayo_fondos',
            'mayo_solicitado',
            'junio_anio',
            'junio_fondos',
            'junio_solicitado',
            'julio_anio',
            'julio_fondos',
            'julio_solicitado',
            'agosto_anio',
            'agosto_fondos',
            'agosto_solicitado',
            'septiembre_anio',
            'septiembre_fondos',
            'septiembre_solicitado',
            'octubre_anio',
            'octubre_fondos',
            'octubre_solicitado',
            'noviembre_anio',
            'noviembre_fondos',
            'noviembre_solicitado',
            'diciembre_anio',
            'diciembre_fondos',
            'diciembre_solicitado',
            'total_anio',
            'total_fondos',
            'total_solicitado',
            'total_aprobado',
        ]);
    } else {
        editor.disable([
            'centro_costos',
            'descripcion_centro_costos',
            'partida',
            'descripcion_partida',
            'enero_anio',
            'enero_fondos',
            'enero_solicitado',
            'enero_aprobado',
            'febrero_anio',
            'febrero_fondos',
            'febrero_solicitado',
            'febrero_aprobado',
            'marzo_anio',
            'marzo_fondos',
            'marzo_solicitado',
            'marzo_aprobado',
            'abril_anio',
            'abril_fondos',
            'abril_solicitado',
            'abril_aprobado',
            'mayo_anio',
            'mayo_fondos',
            'mayo_solicitado',
            'mayo_aprobado',
            'junio_anio',
            'junio_fondos',
            'junio_solicitado',
            'junio_aprobado',
            'julio_anio',
            'julio_fondos',
            'julio_solicitado',
            'julio_aprobado',
            'agosto_anio',
            'agosto_fondos',
            'agosto_solicitado',
            'agosto_aprobado',
            'septiembre_anio',
            'septiembre_fondos',
            'septiembre_solicitado',
            'septiembre_aprobado',
            'octubre_anio',
            'octubre_fondos',
            'octubre_solicitado',
            'octubre_aprobado',
            'noviembre_anio',
            'noviembre_fondos',
            'noviembre_solicitado',
            'noviembre_aprobado',
            'diciembre_anio',
            'diciembre_fondos',
            'diciembre_solicitado',
            'diciembre_aprobado',
            'total_anio',
            'total_fondos',
            'total_solicitado',
            'total_aprobado',
        ]);
    }


    $('#datos_tabla').DataTable({
        dom: "lfrtip",
        columns: [
            {data: "centro_costos"},
            {data: "descripcion_centro_costos"},
            {data: "partida"},
            {data: "descripcion_partida"},

            {data: "enero_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "enero_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "enero_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "enero_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

            {data: "febrero_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "febrero_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "febrero_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "febrero_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

            {data: "marzo_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "marzo_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "marzo_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "marzo_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

            {data: "abril_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "abril_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "abril_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "abril_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

            {data: "mayo_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "mayo_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "mayo_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "mayo_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

            {data: "junio_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "junio_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "junio_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "junio_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

            {data: "julio_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "julio_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "julio_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "julio_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

            {data: "agosto_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "agosto_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "agosto_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "agosto_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

            {data: "septiembre_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "septiembre_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "septiembre_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "septiembre_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

            {data: "octubre_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "octubre_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "octubre_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "octubre_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

            {data: "noviembre_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "noviembre_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "noviembre_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "noviembre_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

            {data: "diciembre_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "diciembre_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "diciembre_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "diciembre_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},

            {data: "total_anio", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "total_fondos", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "total_solicitado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
            {data: "total_aprobado", render: $.fn.dataTable.render.number(',', '.', 2, '$')},
        ],
        keys: {
            columns: ':not(:first-child)',
            editor: editor
        },
        select: {
            style: 'os',
            selector: 'td:first-child',
            blurable: true
        },
        buttons: [
            {extend: "create", editor: editor},
            {extend: "edit", editor: editor},
            {extend: "remove", editor: editor}
        ],
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "�ltimo",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        columnDefs: [{
            render: function (data, type, row) {
                return '<a data-toggle="modal" data-target=".modal_ver_detalle" data-tooltip="Ver">' + data + '</a>';
            },
            targets: 0
        }, {
            visible: false,
            targets: [3, 1]
        }]
    });
}

$("#datos_tabla tbody").on('click','tr', function () {


    var id = $(this).attr('id');


    var centro_costos = $('tr#'+ id).find('td:first').text();
    var partida = $('tr#'+ id).find('td:nth-child(2)').text();


    $.ajax({
        method: "POST",
        url: "/anteproyecto/descripcion",
        data: {partida: partida, centro_costos: centro_costos}
    })
    .done(function( data ) {
            console.log(data);
        $('#detalle_partida').text(data.descripcion_partida);
        $('#detalle_centro_costos').text(data.descripcion_centro_costos);
        $('#no_partida').text(data.partida);
    });

});


$("#boton_cerrar_presupuesto").on( "click", function() {

    $.ajax({
        url: '/anteproyecto/cerrar_presupuesto_aprobado',
        method: 'POST',
        dataType: 'json',
        success: function(s){
            location.reload();
        },
        error: function(e){
            console.log(e.responseText);
        }
    });

});

$("#boton_abrir_presupuesto").on( "click", function() {

    $.ajax({
        url: '/anteproyecto/abrir_presupuesto_aprobado',
        method: 'POST',
        dataType: 'json',
        success: function(s){
            location.reload();
        },
        error: function(e){
            console.log(e.responseText);
        }
    });

});