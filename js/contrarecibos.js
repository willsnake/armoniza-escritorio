/**
 * Created by Daniel on 17-Dec-14.
 */
var tipo_contrarecibo;
var datos_tabla;

$(document).ready(function() {
    $('.datos_tabla').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('.datos_tabla_aprobado').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('.datos_tabla_pendiente').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    $('.datos_tabla_cancelado').dataTable({
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    //setInterval(refrescarIndice, 500);

});

$('.datos_tabla tbody').on( 'click', 'tr', function () {
    datos_tabla = $('.datos_tabla').DataTable().row( this ).data();
});

$('.modal_borrar').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#cancelar_contrarecibo').val(datos_tabla[0]);
});

$('.modal_poliza').on('show.bs.modal', function (event) {
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#numero_contrarecibo_poliza').html(''+datos_tabla[0]);
    modal.find('#numero_contrarecibo_poliza_hidden').val(datos_tabla[0]);
});

$("#elegir_cancelar_contrarecibo").click(function() {
    var contrarecibo = $("#cancelar_contrarecibo").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/cancelar_contrarecibo_caratula",
        data: {
            contrarecibo: contrarecibo
        }
    })
        .done(function( data ) {
            $("#resultado_borrar").html(data.mensaje);
            $('#modal_resultado_borrar').modal('show');
            table.ajax.reload();
        })
        .fail(function(e) {
            console.log(e);
            // If there is no communication between the server, show an error
            console.log("Error");
            //alert( "error occured" );
        });
});

$("#elegir_generar_poliza").click(function() {
    var contrarecibo = $("#numero_contrarecibo_poliza_hidden").val();

    var table = $('.datos_tabla').DataTable();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/ciclo/generar_poliza_contrarecibo",
        data: {
            contrarecibo: contrarecibo
        }
    })
        .done(function( data ) {
            $("#mensaje_resultado_poliza").html(data.mensaje);
            $('.resultado_poliza').modal('show');
            table.ajax.reload();
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

function BtnTodas() {
    document.getElementById('tab-todas').style.display='block';
    document.getElementById('tab-aprobado').style.display='none';
    document.getElementById('tab-pendiente').style.display='none';
    document.getElementById('tab-cancelado').style.display='none';
    document.getElementById('btn-todas').className='active';
    document.getElementById('btn-aprobado').className='noactive';
    document.getElementById('btn-pendiente').className='noactive';
    document.getElementById('btn-cancelado').className='noactive';
}
function BtnAprobado() {
    document.getElementById('tab-aprobado').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-pendiente').style.display='none';
    document.getElementById('tab-cancelado').style.display='none';
    document.getElementById('btn-aprobado').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-pendiente').className='noactive';
    document.getElementById('btn-cancelado').className='noactive';
}
function BtnPendiente() {
    document.getElementById('tab-pendiente').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-aprobado').style.display='none';
    document.getElementById('tab-cancelado').style.display='none';
    document.getElementById('btn-pendiente').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-aprobado').className='noactive';
    document.getElementById('btn-cancelado').className='noactive';
}
function BtnCancelado() {
    document.getElementById('tab-cancelado').style.display='block';
    document.getElementById('tab-todas').style.display='none';
    document.getElementById('tab-aprobado').style.display='none';
    document.getElementById('tab-pendiente').style.display='none';
    document.getElementById('btn-cancelado').className='active';
    document.getElementById('btn-todas').className='noactive';
    document.getElementById('btn-aprobado').className='noactive';
    document.getElementById('btn-pendiente').className='noactive';
}